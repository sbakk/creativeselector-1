from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.simple import direct_to_template
from django.db.models import get_model
from django.db.models.fields.related import ForeignKey
from django import forms
import logging


from filetransfers.api import prepare_upload, serve_file

def reupload_handler_inline(request, pk, inline_model_info, model_info, field_name):
    model_name = model_info.split(".")
    model_class = get_model(model_name[0], model_name[1])
    
    inline_model_name = str(inline_model_info).split(".")
    inline_model_class = get_model(inline_model_name[0], inline_model_name[1])
    parent = get_object_or_404(model_class, pk=pk)

    class UploadForm(forms.ModelForm):
        class Meta:
            model = inline_model_class
    
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        upload = form.save()
        return HttpResponseRedirect(reverse("adminupload.views.close_popup"))
 
    
    upload_url, upload_data = prepare_upload(request, reverse('adminupload.views.reupload_handler_inline', args=[pk, inline_model_info, model_info, field_name]))
    
    foreign_key_field_name = None
    
    for f in inline_model_class._meta.fields:
      if isinstance(f,ForeignKey):
        foreign_key_field_name = f.name
    
        
    form = UploadForm(initial={foreign_key_field_name: parent.id})
  
    
    return direct_to_template(request, 'upload/reupload.html',
        {'form': form, 'upload_url': upload_url, 'upload_data': upload_data})
        

def reupload_inline_single(request, pk, inline_model_info, model_info, field_name):
    model_name = model_info.split(".")
    model_class = get_model(model_name[0], model_name[1])
    
    inline_model_name = str(inline_model_info).split(".")
    inline_model_class = get_model(inline_model_name[0], inline_model_name[1])
    parent = get_object_or_404(model_class, pk=pk)

    class UploadForm(forms.ModelForm):
        class Meta:
            model = inline_model_class
    
    if request.method == 'POST':
        request.session['uploaded'] = True;
        images = getattr(parent, '%s_set' % inline_model_name[1]).all()
        for i in images:
          i.delete()
        form = UploadForm(request.POST, request.FILES)
        upload = form.save()
        return HttpResponseRedirect(reverse("adminupload.views.close_popup"))
 
    
    upload_url, upload_data = prepare_upload(request, reverse('adminupload.views.reupload_inline_single', args=[pk, inline_model_info, model_info, field_name]))
    
    foreign_key_field_name = None
    
    for f in inline_model_class._meta.fields:
      if isinstance(f,ForeignKey):
        foreign_key_field_name = f.name
    
        
    form = UploadForm(initial={foreign_key_field_name: parent.id})
  
    
    return direct_to_template(request, 'upload/reupload.html',
        {'form': form, 'upload_url': upload_url, 'upload_data': upload_data})        
        


def reupload_handler(request, pk, model_info, field_name):
    model_name = model_info.split(".")
    model_class = get_model(model_name[0], model_name[1])
    
    class UploadForm(forms.ModelForm):
        class Meta:
            model = model_class
    
    upload = get_object_or_404(model_class, pk=pk)
    data = get_object_or_404(model_class, pk=pk)
    
    field = getattr(upload, field_name)
     
    if request.method == 'POST':
        if field.name is not None and len(field.name) > 0:
              field.delete()
        form = UploadForm(request.POST, request.FILES, instance = upload)
        if model_name[1]=='usernofaceprofile':
            if form.is_valid():
                form.save(commit=False)
                upload.image=form.cleaned_data['image']
                upload.save(False)
        elif model_name[1]=='tender':
            if form.is_valid():
                form.save(commit=False)
                data.main_pic=form.cleaned_data['main_pic']
                data.save()
            else:
                logging.error(form.errors)
                return direct_to_template(request,'message.html', {'title': 'hiba', 'message': 'Hiba: %s <br>kerlek ertesid az admint' % (form.errors)})
        else:
            form.save()
        return HttpResponseRedirect(reverse("adminupload.views.close_popup"))
 
    upload_url, upload_data = prepare_upload(request, request.path)
    form = UploadForm(instance = upload)
    return direct_to_template(request, 'upload/reupload.html',
        {'form': form, 'upload_url': upload_url, 'upload_data': upload_data,
         'upload': upload})
         

    
def download_handler(request, pk, model_info, field_name):
    model_name = model_info.split(".")
    model_class = get_model(model_name[0], model_name[1])
    upload = get_object_or_404(model_class, pk=pk)
    field = getattr(upload, field_name)
    try:
      return serve_file(request, field, field_name=field_name, save_as=True)
    except:
      return HttpResponse()
    
def close_popup(request):
    return direct_to_template(request, 'upload/close_popup.html', {})
    

