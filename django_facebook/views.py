from django.contrib.auth import authenticate, login
from django.contrib import auth
from django.http import HttpResponseRedirect
from django_facebook.view_decorators import fashiolista_env
from django.contrib.auth.models import User
from creativeselector.models import FacebookProfile, NameExport, Staticpage
from creativeselector.views import render_response

@fashiolista_env
def connect(request):
    '''
    Connect a user if it is authenticated
    
    Else
        Login or registers the given facebook user
        Redirects to a registration form if the data is invalid or incomplete
    '''
    facebook_login = bool(int(request.REQUEST.get('facebook_login', 0)))
    if facebook_login:
        facebook = request.facebook()
        #facebook = facebook_[0]
        if facebook.is_authenticated():
            profile = facebook.facebook_profile_data()
            #profile=profil[0]
            if request.user.is_authenticated():
                return _connect_user(request, facebook)
            else:
                kwargs = {}
                if not "id" in profile:
                    #return render_response(request, 'message.html', {'title': 'hiba tortent', 'message': 'Profil: %s, cookies: %s' % (profile,facebook_[1])})
                  return HttpResponseRedirect(request.session['HTTP_REFERER'])
                authenticated_user = authenticate(facebook_id=profile['id'], **kwargs)
                if authenticated_user:
                    return _login_user(request, facebook, authenticated_user, update=getattr(authenticated_user, 'fb_update_required', False))
                else:
                    request.method="GET"
                    return register_user(request)
        else:
            return HttpResponseRedirect(request.session['HTTP_REFERER'])
#def logout(request):
    #logout(request)
    #return HttpResponseRedirect("/")

def _connect_user(request, facebook):
    if not request.user.is_authenticated():
        raise ValueError, 'Connect user can only be used on authenticated users'
    if facebook.is_authenticated():
        facebook_data = facebook.facebook_registration_data()
        profile = request.user.get_profile()
        user = request.user
        #update the fields in the profile
        profile_fields = profile._meta.fields
        user_fields = user._meta.fields
        profile_field_names = [f.name for f in profile_fields]
        user_field_names = [f.name for f in user_fields]
        facebook_fields = ['facebook_id', 'first_name', 'last_name']

        for f in facebook_fields:
            facebook_value = facebook_data.get(f, False)
            if facebook_value:
                if f in profile_field_names and not getattr(profile, f, False):
                    setattr(profile, f, facebook_value)
                elif f in user_field_names and not getattr(user, f, False):
                    setattr(user, f, facebook_value)

        profile.save()
        user.save()

    return HttpResponseRedirect(request.session['HTTP_REFERER'])


def _login_user(request, facebook, authenticated_user, update=False):
    login(request, authenticated_user)
    
    if facebook.is_authenticated():
        request.context['facebook_data'] = facebook_data = facebook.facebook_registration_data()
        if request.user.is_authenticated() and not request.user.email and facebook_data["email"]:
          request.user.email = facebook_data["email"]
          request.user.save()

    if update:
        _connect_user(request, facebook)

    return HttpResponseRedirect(request.session['HTTP_REFERER'])
    

@fashiolista_env
def register_user(request):
    request.context['facebook_mode'] = True
    facebook_data = {}
    facebook = request.facebook()
    if facebook.is_authenticated():
        request.context['facebook_data'] = facebook_data = facebook.facebook_registration_data()
  
    if request.method == 'POST':
        try:
            usr = User.objects.get(email__exact=facebook_data["email"])
        except:
            usr = None
        if not usr:
            user = User(username = facebook_data['username'],
                        first_name = facebook_data['first_name'],
                        last_name = facebook_data['last_name'],
                        email=facebook_data["email"])
            user.set_unusable_password()
            user.save()
            try:
                ne = NameExport.objects.get(email__exact=user.email)
            except:
                ne = None
            if not ne:
                ne = NameExport(userid=user.id, username=user.username, first_name=user.first_name, last_name=user.last_name, email=user.email)
                ne.save()    
        else:
            user=usr
        profile = FacebookProfile(facebook_id = facebook_data['id'], 
                            name = u"%s %s" % (user.first_name, user.last_name),
                            image = facebook_data['image'],
                            thumb = facebook_data['image_thumb'],
                            profile_link = facebook_data['facebook_profile_url']
                            )
        profile.user = user
        profile.save()
        kwargs = {}
        authenticated_user = authenticate(facebook_id=facebook_data['id'], **kwargs)
        login(request, authenticated_user)
        return HttpResponseRedirect("/profilom")
    else:
        try:
            tos = Staticpage.objects.get(link = u"felhasznalasi_feltetelek")
        except:
            tos = None
        return render_response(request, 'facebook/setup.html', {
              'image':  facebook_data['image'],
              'name': u"%s %s" % (facebook_data['first_name'], facebook_data['last_name']),
              'tos' : tos,
              'next': request.session['HTTP_REFERER']
              }, {})

