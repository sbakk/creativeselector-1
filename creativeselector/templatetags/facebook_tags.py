# Copyright 2008-2009 Brian Boyer, Ryan Mark, Angela Nitzke, Joshua Pollock,
# Stuart Tiffen, Kayla Webley and the Medill School of Journalism, Northwestern
# University.
#
# This file is part of django-facebookconnect.
#
# django-facebookconnect is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# django-facebookconnect is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with django-facebookconnect.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.conf import settings
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.contrib.auth import REDIRECT_FIELD_NAME
from urllib import quote_plus
import logging
from creativeselector.models import Tender, UserNoFaceProfile
from django.contrib.auth.models import User

register = template.Library()
    
@register.inclusion_tag('facebook/js.html')
def initialize_facebook_connect():
    return {'facebook_api_key': settings.FACEBOOK_APP_ID}

@register.inclusion_tag('facebook/show_string.html', takes_context=True)
def show_facebook_photo(context, user, height="22", size = "n"):
    image = "/media/images/t_silhouette_logo.gif"
    if not user:
      return {'string': '<img src="%s" height="%s"/>' % (image, height)}
    try:
        if size=='q':
            if user.get_profile().thumb:
                image = user.get_profile().thumb
        else:
            if user.get_profile().image:
                image = user.get_profile().image
    except:
        try:
            nf= UserNoFaceProfile.objects.get(user=user)
            if nf.image:
                image = '/upload/download/%s/creativeselector.usernofaceprofile/image/' % (nf.pk)
            else:
                image = "/media/images/t_silhouette_logo.gif"
        except:
            image = "/media/images/t_silhouette_logo.gif"
    #try:
      #return {'string': '<a href="%s" target="_blank"><img src="%s" height="%s"/></a>' % (user.get_profile().profile_link, image, height)}
    #except:
    return {'string': '<img src="%s" height="%s"/>' % (image, height)}


@register.inclusion_tag('facebook/show_string.html', takes_context=True)
def show_facebook_name(context, user):
    try:
      return {'string': '<a href="%s" target="_blank">%s</a>' % (user.get_profile().profile_link, user.get_profile().name)}
    except:
      return {'string': ""}


        
@register.inclusion_tag('facebook/show_string.html', takes_context=True)
def facebook_comments_xid(context):
  return {'string': quote_plus(context['full_url'])}


@register.inclusion_tag('facebook/show_string.html', takes_context=True)
def facebook_comments_xid_url(context, idea):
  return {'string': quote_plus("http://www.creativeselector.hu/otlet/%s" % idea.link)}
    
@register.inclusion_tag('facebook/connect_button.html', takes_context=True)
def show_connect_button(context):
    return {}

    from django import template

    register = template.Library()

@register.filter
# truncate after a certain number of characters
def truncchar(value, arg):
    if len(value) < arg:
        return value
    else:
        return value[:arg] + '...'

from google.appengine.api.labs import taskqueue
@register.inclusion_tag('facebook/show_string.html', takes_context=True)
def task_queue_length(context):
  return {'string': "%s" % taskqueue}


  
