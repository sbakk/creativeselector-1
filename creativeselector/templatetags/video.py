from django.template.defaultfilters import stringfilter
from django import template
import re

register = template.Library()

#####################
# YOUTUBE
#####################
youtube_regex = re.compile("((http|https)://)?(www\.)?(youtube\.com/watch\?v=)(?P<video_id>[A-Za-z0-9\-=_]{11})([^\s|\<]*)",  re.VERBOSE)
vimeo_regex = re.compile("(http://)?(www\.)?(vimeo\.com/)(?P<video_id>[0-9]*)([^\s|\<]*)",  re.VERBOSE)

youtube_player = """
      <iframe width="330" height="254" src="http://www.youtube.com/embed/\g<video_id>?rel=0&fmt=18&wmode=transparent" frameborder="0" allowfullscreen></iframe>
"""
vimeo_player = """
    <iframe src="http://player.vimeo.com/video/\g<video_id>?title=0&amp;byline=0&amp;portrait=0"
    width="330" height="186" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
"""



@register.filter
@stringfilter
def video(url):
    if url.find('vimeo')>=0:
        result = vimeo_regex.sub(vimeo_player, url)
    else:
        result = youtube_regex.sub(youtube_player, url)
    return result
    






