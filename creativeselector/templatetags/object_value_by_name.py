from django import template  
  
register = template.Library()  
  
@register.filter(name='object_value_by_name')  
def object_value_by_name(obj,name): 
  try:
    return getattr(obj,name) 
  except:
    return ""

@register.filter(name='filename') 
def filename(field):
  try:
    return field._get_file().name.rsplit('/', 1)[-1]
  except:
    return ""
