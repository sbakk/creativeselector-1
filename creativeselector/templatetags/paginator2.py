from django import template
from django.template.loader import get_template
from django.conf import settings

import tokenize
import StringIO

register = template.Library()



def process_ranges(ranges, page):
    range_count = len(ranges)
    page = int(page)
    if (range_count>16):
      if (page>2 and page < range_count-1):
        middle = page
      else:
        middle = int(round(range_count/2))
      
      result=[]
      
      if (page<6):
        for i in range(1, page+3):
         result.extend([i])
        result.extend(['...', range_count-1, range_count])
      
      elif (page>=6 and page<range_count-4):
        result.extend([1,2,'...'])
        for i in range(middle-2, middle+3):
         result.extend([i])
        result.extend(['...', range_count-1, range_count])
        
      elif (page>=range_count-4):
        result.extend([1,2,'...'])
        for i in range(page-2, range_count+1):
         result.extend([i])
 
      return result
    else:
      return ranges

@register.inclusion_tag('paginator.html', takes_context=True)
def paginator(context, paginator_name, adjacent_pages):
     paginator = context[paginator_name]
     paginator_infix = ""
     try:
       paginator_infix = context['paginator_infix']
     except:
       pass
     page = context['page']

     if (paginator.count>0):
        ranges = process_ranges(paginator.page_range, page)
     else:
        ranges = None
    
     if (page<paginator.num_pages):
       next = page+1
     else:
       next = None

     if (page>1):
       prev = page-1
     else:
       prev = None 

     range_min = (page-1)*adjacent_pages+1
     range_max = page*adjacent_pages+1
     range_full = paginator.count

     if (range_max>range_full):
       range_max = range_full
        
        
     return  {
        'paginator': paginator,
        'paginator_name': paginator_name,
        'paginator_infix': paginator_infix,
        'ranges': ranges,
        'page': page,
        'next': next,
        'prev': prev,
        'range_min': range_min,
        'range_max': range_max,
        'range_full': range_full
    }
    




