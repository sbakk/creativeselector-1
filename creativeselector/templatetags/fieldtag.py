from django import template  
  
register = template.Library()  
  
@register.filter(name='field_value')  
def field_value(value):  
    if value:
        return value
    return ""
  
@register.filter(name='field_type')  
def field_type(value):  
    return value.field.__class__.__name__  
   
@register.filter(name='widget_type')  
def widget_type(value):  
    return value.field.widget.__class__.__name__
    
    
@register.filter(name='is_image')
def is_image(value):
    return unicode(value).lower().endswith('jpg') or value.lower().endswith('gif') or value.lower().endswith('png') or value.lower().endswith('jpeg')
     
@register.filter(name='prepare_model_info')     
def prepare_model_info(model_name, app_info):
    value = str(app_info)
    helper = value.split('.')
    
    return "%s.%s" % (helper[0][1:], model_name.__class__.__name__.lower())  