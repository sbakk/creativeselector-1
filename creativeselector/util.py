# -*- coding: utf-8 -*-
from google.appengine.api import mail
from django.template import Context, loader
from django.core.management import setup_environ
from math import ceil
from django import forms
from django.db import models
from django.template.loader import render_to_string
from django.forms.widgets import Select, MultiWidget, DateInput, TextInput
from time import strftime
import urllib, datetime
import binascii, base64
from pyDes import *


###################################
# SEND MAIL
###################################
def send_mail(from_address, to_address, subject, body_template, html_template, c, attachments=None):
    html_body = loader.get_template(html_template + ".html")
    body = loader.get_template(body_template + ".html")
    
    if attachments:
      mail.send_mail(
        sender=from_address,
        to=to_address,
        subject=subject,
        body=body.render(Context(c)),
        html=html_body.render(Context(c)),
        attachments=attachments
      );
    else:
      mail.send_mail(
        sender=from_address,
        to=to_address,
        subject=subject,
        body=body.render(Context(c)),
        html=html_body.render(Context(c))
      );

###################################
# SLUGIFY
###################################
      
import re
from string import maketrans

_NORMALIZE = re.compile(r'\W+', re.UNICODE)

def slugify(text):
    """A unicode-aware alternative to Django's slugify"""
#    trans = maketrans("áéíöőóúüű", u"aeiooouuu")
   
    result = _NORMALIZE.sub('_', text).strip('_').lower()
    result =  result.replace(u"á", u"a").replace(u"í", u"i").replace(u"é", u"e").replace(u"ű", u"u").replace(u"ü", u"u").replace(u"ú", u"u").replace(u"ő", u"o").replace(u"ö", u"o").replace(u"ó", u"o")
    return result
    
###################################
# MY FAST PAGINATOR
###################################
class Paginator(object):
    def __init__(self, object_count, per_page, orphans=0, allow_empty_first_page=True):
        self.object_count = object_count
        self.per_page = per_page
        self.orphans = orphans
        self.allow_empty_first_page = allow_empty_first_page
        self._num_pages = self._count = None

    def validate_number(self, number):
        "Validates the given 1-based page number."
        try:
            number = int(number)
        except ValueError:
            raise PageNotAnInteger('That page number is not an integer')
        if number < 1:
            raise EmptyPage('That page number is less than 1')
        if number > self.num_pages:
            if number == 1 and self.allow_empty_first_page:
                pass
            else:
                raise EmptyPage('That page contains no results')
        return number

    def page(self, number):
        "Returns a Page object for the given 1-based page number."
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        return Page(bottom, top, number, self)

    def _get_count(self):
        "Returns the total number of objects, across all pages."
        if self._count is None:
          self._count = self.object_count
        return self._count
    count = property(_get_count)

    def _get_num_pages(self):
        "Returns the total number of pages."
        if self._num_pages is None:
            if self.count == 0 and not self.allow_empty_first_page:
                self._num_pages = 0
            else:
                hits = max(1, self.count - self.orphans)
                self._num_pages = int(ceil(hits / float(self.per_page)))
        return self._num_pages
    num_pages = property(_get_num_pages)

    def _get_page_range(self):
        """
        Returns a 1-based range of pages for iterating through within
        a template for loop.
        """
        return range(1, self.num_pages + 1)
    page_range = property(_get_page_range)

QuerySetPaginator = Paginator # For backwards-compatibility.

class Page(object):
    def __init__(self, bottom, top, number, paginator):
        self.bottom = bottom
        self.top = top
        self.number = number
        self.paginator = paginator

    def __repr__(self):
        return '<Page %s of %s>' % (self.number, self.paginator.num_pages)

    def has_next(self):
        return self.number < self.paginator.num_pages

    def has_previous(self):
        return self.number > 1

    def has_other_pages(self):
        return self.has_previous() or self.has_next()

    def next_page_number(self):
        return self.number + 1

    def previous_page_number(self):
        return self.number - 1

    def start_index(self):
        """
        Returns the 1-based index of the first object on this page,
        relative to total objects in the paginator.
        """
        # Special case, return zero if no items.
        if self.paginator.count == 0:
            return 0
        return (self.paginator.per_page * (self.number - 1)) + 1

    def end_index(self):
        """
        Returns the 1-based index of the last object on this page,
        relative to total objects found (hits).
        """
        # Special case for the last page because there can be orphans.
        if self.number == self.paginator.num_pages:
            return self.paginator.count
        return self.number * self.paginator.per_page

#########################################################
# Fizetési tranzakciót kezelő üzenetes osztály
#########################################################

class CibPayTrans():
    def __init__(self, uid, callbackurl, trid, amount):
        self.pid="NWN0001"
        self.uid=uid
        self.cur="HUF"
        self.trid=trid
        self.amo=amount
        self.ts=None
        self.auth="0"
        self.lang="HU"
        self.url=callbackurl
        self.key=None
        self.iv=None
        #self.nvn='EKI  NWN N·¸$ŐmQŐm›µJJ|’y8Ö#¸·NxV4'
        self.setkeys()

    def setkeys(self):
        #f= open("/NWN.des","r")
        #d=f.read()
        #f.close()
        # teszt:'454b490000024e574e004eb7b824d56d51d56d9bb54a014a137c927938d623b8b74e78563412'  éles:'454b490000024e574e004eb7b8290bc767fd983ed9b96bc7bc0bc7d60df125b8b74e78563412
        f='454b490000024e574e004eb7b8290bc767fd983ed9b96bc7bc0bc7d60df125b8b74e78563412'
        d=binascii.a2b_hex(f)
        k1=d[14:22]
        k2=d[22:30]
        self.iv=d[30:]
        self.key=k1+k2+k1

    def set_lang(self, lng):
        self.lang=lng
    
    def msg10(self): #inicializálás
        now=datetime.datetime.now()
        self.ts=now.strftime("%Y%m%d%H%M%s")
        data="CRYPTO=1&MSGT=10&PID=%s&TRID=%s&UID=%s&AMO=%s&CUR=%s&TS=%s&AUTH=%s&LANG=%s&URL=%s" % (self.pid,self.trid,self.uid,self.amo,self.cur,self.ts,self.auth,self.lang,self.url)
        send_data = self.encode(data)
        return send_data

    def msg20(self): #átirányítás kérelem
        data="CRYPTO=1&MSGT=20&TRID=%s&PID=%s" % (self.trid,self.pid)
        send_data = self.encode(data)
        return send_data

    def msg32(self): #sikeres esetén lezárás kérelem
        data="CRYPTO=1&MSGT=32&TRID=%s&PID=%s&AMO=%s" % (self.trid,self.pid,self.amo)
        send_data = self.encode(data)
        return send_data

    def msg33(self): #time out esetén lezárás kérés
        data="CRYPTO=1&MSGT=33&TRID=%s&PID=%s&AMO=%s" % (self.trid,self.pid,self.amo)
        send_data = self.encode(data)
        return send_data

    def answer11(self, text): #### inicializálásra válasz
        ans=self.decode(text)
        inf=ans.split('&')
        msgt=''
        pid=''
        trid=''
        rc=''
        for i in inf:
            if i[:5].upper() == 'MSGT=':
                msgt=i[5:]
            if i[:4].upper() == 'PID=':
                pid=i[4:]
            if i[:5].upper() == 'TRID=':
                trid=i[5:5+16]
            if i[:3].upper() == 'RC=':
                rc=i[3:]
        if pid!=self.pid:
            pid=None
        if trid!=str(self.trid):
            trid=None
        return [msgt,pid,trid,rc]

    def answer21(self, text): ###visszatért fizetés utáni
        ans=self.decode(text)
        inf=ans.split('&')
        msgt=''
        pid=''
        trid=''        
        for i in inf:
            if i[:5].upper() == 'MSGT=':
                msgt=i[5:]
            if i[:4].upper() == 'PID=':
                pid=i[4:]
            if i[:5].upper() == 'TRID=':
                trid=i[5:5+16]
        if pid!=self.pid:
            pid=None
        #if trid!=str(self.trid):
            #trid=None
        return [msgt,pid,trid]

    def answer31(self, text): ###lezáró üzenet
        ans=self.decode(text)
        inf=ans.split('&')
        msgt=''
        pid=''
        trid=''
        rc=''
        rt=''
        anum=''
        amo=''
        for i in inf:
            if i[:5].upper() == 'MSGT=':
                msgt=i[5:]
            if i[:4].upper() == 'PID=':
                pid=i[4:]
            if i[:5].upper() == 'TRID=':
                trid=i[5:5+16]
            if i[:3].upper() == 'RC=':
                rc=i[3:]
            if i[:3].upper() == 'RT=':
                rt=i[3:]
            if i[:5].upper() == 'ANUM=':
                anum=i[5:]
            if i[:4].upper() == 'AMO=':
                amo=i[4:]
        if pid!=self.pid:
            pid=None
        if trid!=str(self.trid):
            trid=None
        if amo!=str(self.amo):
            amo=None
        return [msgt,pid,trid,rc,rt,anum,amo]


    def encode(self,text):
        arr=text.split('&')
        outs=''
        pid=''
        for i in arr:
            if i.upper()!="CRYPTO=1":
                outs+="&"+i
            if i[:4].upper()=="PID=":
                pid=i[4:7].upper()+"0001"
        outs=outs[1:]
        outs=urllib.quote_plus(outs)
        outs=outs.replace("%3D","=")
        outs=outs.replace("%26","&")
        crc="%X" % (binascii.crc32(outs) & 0xffffffff)
        crc=crc.zfill(8)
        for i in range(0,4):
            outs+=chr(int(crc[i*2:i*2+2],16))
        pad=8-(len(outs)%8)
        for i in range(0,pad):
            outs+=chr(pad)
        ts=triple_des(self.key, CBC, self.iv)
        outs=ts.encrypt(outs)
        pad=3-(len(outs)%3)
        for i in range(0,pad):
            outs+=chr(pad)
        outs=base64.b64encode(outs)
        outs=urllib.quote_plus(outs)
        outs="PID="+pid+"&CRYPTO=1&DATA="+outs
        return outs

    def decode(self,text):
        arr=text.split('&')
        outs=''
        pid=''
        for i in arr:
            if i[0:5].upper()=="DATA=":
                outs=i[5:]
            if i[:4].upper()=="PID=":
                pid=i[4:7].upper()+"0001"
        outs=urllib.unquote_plus(outs)    
        outs=outs.decode('base64')
        lastc=ord(outs[-1])
        validpad=1
        for i in range(0,lastc):
            if ord(outs[-1-i])!=lastc:
                validpad=0
        if validpad==1:
            outs=outs[0:len(outs)-lastc]
        ts=triple_des(self.key, CBC, self.iv)
        outs=ts.decrypt(outs)
        lastc=ord(outs[-1])
        validpad=1
        for i in range(0,lastc):
            if ord(outs[-1-i])!=lastc:
                validpad=0
        if validpad==1:
            outs=outs[0:len(outs)-lastc]
        crc=outs[-4:]
        crch=''
        for i in range(0,4):
            crch+=("%X" % ord(crc[i])).zfill(2)
        outs=outs[0:-4]
        crc="%X" % (binascii.crc32(outs) & 0xffffffff)
        crc=crc.zfill(8)
        outs=outs.replace("&","%26")
        outs=outs.replace("=","%3D")
        outs=urllib.unquote_plus(outs)
        outs="CRYPTO=1&"+outs
        return outs

