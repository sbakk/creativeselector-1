# -*- coding: utf-8 -*-
import datetime
import urllib
import logging
from django.contrib.auth.models import User, UserManager
from django.db import models
from django.utils.encoding import smart_str
from django.utils.hashcompat import md5_constructor, sha_constructor
from django.utils.translation import ugettext_lazy as _
from creativeselector.util import send_mail, slugify
from django.db.models import permalink
from django.contrib.sitemaps import ping_google
from django.db.models import Max
import random
from django.db import models
from django.conf import settings



#################################################
# HELPER METHOS
#################################################

UNUSABLE_PASSWORD = '!' # This will never be a valid hash

def get_hexdigest(algorithm, salt, raw_password):
    """
    Returns a string of the hexdigest of the given plaintext password and salt
    using the given algorithm ('md5', 'sha1' or 'crypt').
    """
    raw_password, salt = smart_str(raw_password), smart_str(salt)
    if algorithm == 'crypt':
        try:
            import crypt
        except ImportError:
            raise ValueError('"crypt" password algorithm not supported in this environment')
        return crypt.crypt(raw_password, salt)

    if algorithm == 'md5':
        return md5_constructor(salt + raw_password).hexdigest()
    elif algorithm == 'sha1':
        return sha_constructor(salt + raw_password).hexdigest()
    raise ValueError("Got unknown password algorithm type in password.")

def check_password(raw_password, enc_password):
    """
    Returns a boolean of whether the raw_password was correct. Handles
    encryption formats behind the scenes.
    """
    algo, salt, hsh = enc_password.split('$')
    #logging.error("PASSS CHECK: %s = %s" % (hsh, get_hexdigest(algo, salt, raw_password)))
    return hsh == get_hexdigest(algo, salt, raw_password)

def sitemap_ping():
    try:
        pass
        #ping_google()
    except Exception:
        # Bare 'except' because we could get a variety
        # of HTTP-related exceptions.
        pass


#################################################
# PROX MODELS TO SEPARATE IN THE ADMIN THE USER MODELS
#################################################

class Staff(User):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = 'Staff account'
        verbose_name_plural = 'Staff accounts'

  
class SiteMeta(models.Model):
    link = models.CharField("link", max_length=200)
    title = models.CharField("Title", max_length=200)
    keywords = models.TextField("Keywords", max_length=1000)
    description = models.TextField("Description", max_length=1000)

    def __unicode__(self):
       return u"%s - %s" % (self.link, self.title)

    class Meta:
      verbose_name = 'Meta adat'
      verbose_name_plural = 'Meta adatok'


#######################################################################
# BLOG       
#######################################################################
from tagging.fields import TagField
from tagging.models import Tag

class BlogEntry(models.Model):
    title = models.CharField("Title", max_length=200)
    link = models.CharField("Link2", max_length=200, blank = True)
    tagss = TagField("Tags")
    content = models.TextField("Content", max_length=1000)
    created = models.DateTimeField(_('created'), default=datetime.datetime.now)
    published = models.BooleanField(_('Published'), default=False)

    def __unicode__(self):
        return self.title 
    
    def _get_tags(self):
        return Tag.objects.get_for_object(self)

    def _set_tags(self, tag_list):
        Tag.objects.update_tags(self, tag_list)

    tags = property(_get_tags, _set_tags)

    def generate_tags(self):
        self.tagss = generate_tags_blog(u"%s" % (self.title))
        super(BlogEntry, self).save()

    def tag_splited(self):
        txt=u"%s" % (self.tagss)
        words=txt.split(" ")
        return words

    @models.permalink
    def get_absolute_url(self):
        return ('creativeselector.views.blogentry', [str(self.link)])

    def save(self, *args, **kwargs):
        if not self.link:
          self.link = slugify(u"%s_%s" %  (self.title, self.created))
        if not self.tagss:
          self.generate_tags()
        super(BlogEntry, self).save(*args, **kwargs)
        sitemap_ping()       

    class Meta:
       verbose_name = 'Blog bejegyzés'
       verbose_name_plural = 'Blog bejegyzések'


class BlogComment(models.Model):
    blog = models.ForeignKey(BlogEntry)
    user = models.ForeignKey(User)
    comment = models.TextField("Comment", max_length=1000)
    slug = models.CharField("Link", max_length=200, blank = True)
    created = models.DateTimeField("Létrehozva", default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(u"%s_%s" %  (self.blog.link, self.created))
        super(BlogComment, self).save(*args, **kwargs)

    class Meta:
       verbose_name = 'Blog komment'
       verbose_name_plural = 'Blog kommentek'
       ordering = ('created',)

def generate_tags_blog(string):
  

  # remove all separator
  string = re.sub(tag_strip_pattern, ' ', string).lower()
  
  # get all words
  words = string.split(" ")

  
  # unify
  words_set = set(words)
  
  
  # empty result
  result = []
  
  # iterate over all words
  for word in words_set:
    if len(word) > 3 and not word.isdigit():
        result.append(word)
  
  result = set(result)
  resultString =  " ".join(result)
  resultString =  resultString[:499]
  
  return resultString

#######################################################################
# STATIC PAGES       
#######################################################################
class Staticpage(models.Model):
   title = models.CharField("Title", max_length=200)
   link = models.CharField("Link", max_length=200, blank = True)
   content = models.TextField("Content", max_length=1000)
   
   def __unicode__(self):
       return self.title 

   @permalink
   def get_absolute_url(self):
       return ('creativeselector.views.staticpage', (),
              { 
              'slug': '%s' % self.link
              }
              )

   def save(self, *args, **kwargs):
       if not self.link:
         self.link = slugify(u"%s" % self.title)
       super(Staticpage, self).save(*args, **kwargs)
       sitemap_ping()
       
   class Meta:
      verbose_name = 'Statikus oldal'
      verbose_name_plural = 'Statikus oldalak'       
       
class BlackWords(models.Model):
    word = models.CharField("Szó", max_length=200)

    def __unicode__(self):
       return self.word

    def save(self, *args, **kwargs):
       super(BlackWords, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Tiltott szó'
        verbose_name_plural = 'Tiltott szavak'
       

#######################################################################
# Tender       
#######################################################################
import logging
import re
tag_strip_pattern = re.compile(r'[\W]+', re.UNICODE)

def check_word_in_whitelist(whitelist, word):
  for item in whitelist:
    item = u"%s" % item
    word = u"%s" % word
    if word in item or item in word:
      return item
  return None
  


def generate_tags_helper(string):
  # get black list
  whiteStatic = Staticpage.objects.get(link = "feher_lista")

  whitelist = re.sub(tag_strip_pattern, ' ', whiteStatic.content).lower()
  whitelist = set(whitelist.split(" "))

  # remove all separator
  string = re.sub(tag_strip_pattern, ' ', string).lower()
  
  # get all words
  words = string.split(" ")

  
  # unify
  words_set = set(words)
  
  
  # empty result
  result = []
  
  # iterate over all words
  for word in words_set:
    if len(word) > 3 and not word.isdigit():
      inlist = check_word_in_whitelist(whitelist, word)
      if inlist:
        result.append(inlist)
  
  result = set(result)
  resultString =  " ".join(result)
  resultString =  resultString[:499]
  
  return resultString
  
#######################################################################
# EXAMPLES      
#######################################################################
class Example(models.Model):
   title = models.CharField("Title", max_length=200)
   link = models.CharField("Link", max_length=200, blank = True)
   content = models.TextField("Content", max_length=1000)
   website = models.CharField("Weboldal", max_length=200, blank = True)
   image = models.FileField("Kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)
   order = models.IntegerField("Sorrend", blank=True, default=0)
   
   def __unicode__(self):
       return self.title 

   def save(self, *args, **kwargs):
       if not self.link:
         self.link = slugify(u"%s" % self.title)
       super(Example, self).save(*args, **kwargs)
       sitemap_ping()

   class Meta:
      verbose_name = 'Példa'
      verbose_name_plural = 'Példák'       

# USER PROFILE
class UserNoFaceProfile(models.Model):
  user = models.OneToOneField(User, related_name="facebook_profile")

  password = models.CharField(u"jelszó*", max_length = 1000)
  password2 = models.CharField(u"jelszó*", max_length = 1000)
  mail = models.CharField("e-mail", max_length=200)
  first_name = models.CharField("Kereszt Név", max_length=200)
  last_name = models.CharField("Vezeték Név", max_length=200)
  image = models.FileField("Kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)  
  profile_link = models.CharField("Profil link", max_length=1000, blank=True, null=True)
    

  def __unicode__(self):
    return "%s %s" % (self.last_name, self.first_name)

  def save(self, new=True,*args, **kwargs):
      if self.password:
          if new:
              random.seed(self.pk)
              serial = random.randrange(0, 100000, 3)
              usr=User(username = "%s_%s_%s" % (self.last_name, self.first_name, serial),
                       first_name = self.first_name,
                       last_name = self.last_name,
                       email = self.mail,
                       password = self.password,
                       is_active = False)
              usr.save()
              self.user = usr
      super(UserNoFaceProfile, self).save(*args, **kwargs)

  def set_password(self, raw_password):
      import random
      algo = 'sha1'
      salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
      hsh = get_hexdigest(algo, salt, raw_password)
      self.password2 = raw_password
      self.password = '%s$%s$%s' % (algo, salt, hsh)

  def check_password(self, password):
      return check_password(password, self.password)
  
  class Meta:
    verbose_name = 'Normál felhasználó'
    verbose_name_plural = 'Normál felhasználók'

class FacebookProfile(models.Model):
  user = models.OneToOneField(User, related_name="facebook_profile")

  facebook_id = models.IntegerField("Facebook azonosító", blank=True, null=True)
  name = models.CharField("Név", max_length=200, blank=True, null=True)
  image = models.CharField("Kép", max_length=1000, blank=True, null=True)
  thumb = models.CharField("Kis kép", max_length=1000, blank=True, null=True)
  profile_link = models.CharField("Profil link", max_length=1000, blank=True, null=True)
    

  def __unicode__(self):
    return self.name

  class Meta:
    verbose_name = 'Facebook felhasználó'
    verbose_name_plural = 'Facebook felhasználók'

class NameExport(models.Model):
  userid = models.IntegerField("Azonosító", blank=True, null=True)

  username = models.CharField("Felh. név", blank=True, null=True, max_length = 1000)
  first_name = models.CharField("Keresztnév", blank=True, null=True, max_length = 1000)
  last_name = models.CharField("Vezetéknév", blank=True, null=True, max_length = 1000)
  email = models.CharField("Email", blank=True, null=True, max_length = 1000)
    
  def __unicode__(self):
    return u"%s %s" % (self.last_name, self.first_name)

  class Meta:
    verbose_name = 'E-mail lista'
    verbose_name_plural = 'E-mail Listák'
   
class SupporterCompany(models.Model):
    name = models.CharField("Title", max_length=200)
    link = models.CharField("Link", max_length=200, blank = True)
    image = models.FileField("Kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.link:
            self.link = slugify(u"%s" %  (self.name))
        super(SupporterCompany, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Támogató cég'
        verbose_name_plural = 'Támogató cégek'

#######################################################################
# NECC       
#######################################################################     

class NeccCategory(models.Model):
  title = models.CharField("Title", max_length=200)
  link = models.CharField("Link", max_length=200, blank = True)
  created = models.DateTimeField('Létrehozva', default=datetime.datetime.now)
  isnew = models.BooleanField('Új', default=False)

  def __unicode__(self):
      return self.title 
      
  @models.permalink
  def get_absolute_url(self):
      return ('creativeselector.views.necc_category', [self.category.link])
           

  def save(self, *args, **kwargs):
      if not self.link:
        self.link = slugify(u"%s" %  (self.title))
      super(NeccCategory, self).save(*args, **kwargs)

  class Meta:
     verbose_name = 'Necc Kategória'
     verbose_name_plural = 'Necc Kategóriák'

class Tender(models.Model):
  owner = models.ForeignKey(User)
  category = models.ForeignKey(NeccCategory, null=True, blank=True)
  
  link = models.CharField("Link", max_length=200, blank = True)
  created = models.DateTimeField("Létrehozva", default=datetime.datetime.now)
  main_pic = models.FileField("Nyito kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)
  
  serial = models.IntegerField("Sorozatszám", blank=True, default=0,  null = True)
  status = models.IntegerField("Státusz", choices=[(1, "feldolgozás alatt"),(2, "elfogadva"), (3, "lejárt")], default=1)

  short_title = models.CharField("Rövid cím", max_length=200, blank = True)
  crowd_no = models.IntegerField("Közösség szám", default = 0, blank=True, null = True)
  introduce = models.TextField("Miről szól?", max_length=1000)
  for_what = models.TextField("Mire való?", max_length=1000, blank=True, null = True)
  budget = models.TextField("Költségvetés?", max_length=1000, blank=True, null = True)
  why_sup = models.TextField("Miért támogasson?", max_length=1000, blank=True, null = True)
  
  full_money = models.IntegerField("Összegyüjtendő összeg", default = 0)
  expiry = models.DateTimeField("Lejárat ideje", blank=True, null = True)
  from_when = models.DateTimeField("Mettől kezdődik", blank=True, null = True)

  visitors = models.IntegerField("Látogatók", default = 0, blank=True, null = True)  
  faceurl = models.CharField("Külső oldal", max_length=800, blank = True, null = True)
  location = models.CharField("Helyszín", max_length=800, blank = True, null = True)
  video_url = models.CharField("video_url", max_length=800, blank = True, null = True)
  tel_no = models.CharField("Telefon szám", max_length=800, blank = True, null = True)
  
  approved = models.BooleanField('Publikus', default=False)
  
  tagss = TagField("Tags",  blank=True, null = True)
  

  def _get_tags(self):
      return Tag.objects.get_for_object(self)

  def _set_tags(self, tag_list):
      Tag.objects.update_tags(self, tag_list)

  tags = property(_get_tags, _set_tags)

  def __unicode__(self):
      return u"%s - %s" % (self.owner, self.short_title) 
  
  @models.permalink
  def get_absolute_url(self):
      return ('creativeselector.views.get_project', [str(self.link)])
 
  def save(self, *args, **kwargs):
      #super(Tender, self).save(*args, **kwargs)
      if not self.link:
          self.link = slugify(u"%s - %s - %s" % (self.owner, self.short_title, self.serial))
          
      if not self.serial:
          random.seed(self.pk)
          serial = random.randrange(0, 100000, 2)
          self.serial = serial
      if not self.crowd_no:
          self.crowd_no=0
      super(Tender, self).save(*args, **kwargs)
      #sitemap_ping()
      
  class Meta:
      verbose_name = 'Projekt'
      verbose_name_plural = 'Projektek'
      ordering = ('-created',)
   
class TenderUpdate(models.Model):
    idea = models.ForeignKey(Tender)
    link = models.CharField("Link", max_length=200, blank = True, null=True)
    content = models.TextField("Content", max_length=1000)
    created = models.DateTimeField("Létrehozva", default=datetime.datetime.now)
    

    def __unicode__(self):
        return u"%s_%s" %(self.idea, self.created)
    
    def save(self, *args, **kwargs):        
        super(TenderUpdate, self).save(*args, **kwargs)
        #sitemap_ping()       

    class Meta:
       verbose_name = 'Projekt Update bejegyzés'
       verbose_name_plural = 'Projekt Update bejegyzések'
       #ordering = ('-created',)

class TenderComment(models.Model):
    idea = models.ForeignKey(Tender)
    user = models.ForeignKey(User)
    comment = models.TextField("Comment", max_length=1000)
    slug = models.CharField("Link", max_length=200, blank = True)
    created = models.DateTimeField("Létrehozva", default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(u"%s_%s" %  (self.idea.link, self.created))
        super(TenderComment, self).save(*args, **kwargs)

    class Meta:
       verbose_name = 'Projekt komment'
       verbose_name_plural = 'Projekt kommentek'
       ordering = ('created',)

class MostTenders(models.Model):
    tender = models.ForeignKey(Tender)
    date = models.DateTimeField("Fizetés időpontja", default=datetime.datetime.now)
    weight = models.IntegerField("Súly", blank=True, default=0,  null = True)
    
    def save(self, *args, **kwargs):
        super(MostTenders, self).save(*args, **kwargs)

    class Meta:
       verbose_name = 'Kiemelt Projekt'
       verbose_name_plural = 'Kiemelt Projektek'

class TenderImage(models.Model):
    image = models.ForeignKey(Tender)  
    file = models.FileField("Fájl", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)
    tit = models.CharField("Cím", max_length=200, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % str(self.file).rsplit('/', 1)[-1]

    class Meta:
      verbose_name = 'Csatolmány'
      verbose_name_plural = 'Csatolmányok'       

class TenderPresent(models.Model):
    tender = models.ForeignKey(Tender)
    min_prize = models.IntegerField("Min díj", blank=True, default=500,  null = True)
    detail = models.TextField("Leírás", max_length=1000, blank=True, null = True)

    def __unicode__(self):
        return u"%s_%s" % (self.tender, self.pk)

    def save(self, *args, **kwargs):
        super(TenderPresent, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Projekt ajándék'
        verbose_name_plural = 'Projekt ajándékok'

class TenderBudget(models.Model):
    tender = models.ForeignKey(Tender)
    owner = models.ForeignKey(User)
    invoice_no = models.CharField("Számla szám", max_length=200)
    money = models.IntegerField("Összegyüjtendő összeg", default = 0)
    money_now = models.IntegerField("Eddig összegyűlt összeg", default = 0, blank=True, null = True)

    def save(self, *args, **kwargs):
        super(TenderBudget, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Projekt számla'
        verbose_name_plural = 'Projekt számlák'
        
class TenderSupporter(models.Model):
    user = models.ForeignKey(User)
    idea = models.ForeignKey(Tender)
    pres = models.ForeignKey(TenderPresent, blank=True, null = True)
    date = models.DateTimeField("Fizetés időpontja", default=datetime.datetime.now)
    money = models.IntegerField("Fizetett összeg", blank=True, null = True)
    anum = models.CharField("Engedélyszám", max_length=16, blank=True, null = True)
    trid = models.CharField("Tranzakció id", max_length=16, blank=True, null = True)
    visibility = models.IntegerField("Láthatóság", choices=[(1, "látható"), (2, "identitás"), (3, "anonymus")], default=1)
    name = models.CharField("Név", max_length=100, blank=True, null = True)
    city = models.CharField("Cím Város", max_length=100, blank=True, null = True)
    adress = models.CharField("Cím utca házszám", max_length=250, blank=True, null = True)
    tellno = models.CharField("Telefonszám", max_length=250, blank=True, null = True)
    zipcode = models.CharField("Irányítószám", max_length=10, blank=True, null = True)
    comment = models.TextField("Megjegyzés", max_length=1000, blank=True, null = True)
    subscribed = models.BooleanField('Frissitési email', default=True)
    get_pres = models.BooleanField('Kapott ajándékot', default=False)
    
    def __unicode__(self):
        if self.pres:
            return u"%s-%s-%s" % (str(self.user), str(self.idea), str(self.pres))
        else:
            return u"%s-%s-ures" % (str(self.user), str(self.idea))

    def newtrid(self):
        trid = str(random.randrange(10000000, 99999999, 2))
        trid += str(random.randrange(10000000, 99999999, 3))
        self.trid = trid      

    def save(self, *args, **kwargs):
      if not self.trid:
          trid = str(random.randrange(10000000, 99999999, 3))
          trid += str(random.randrange(10000000, 99999999, 2))
          self.trid = trid      
      super(TenderSupporter, self).save(*args, **kwargs)
        
    class Meta:
      verbose_name = 'Ötlet Támogató'
      verbose_name_plural = 'Ötlet Támogatók'


class BadTransaction(models.Model):
    idea = models.ForeignKey(Tender)
    pres = models.ForeignKey(TenderPresent, blank=True, null = True)
    supid = models.IntegerField("Támogató azonisító(tendersupporter)", blank=True, null = True)
    date = models.DateTimeField("Fizetés időpontja", blank=True, null = True)
    money = models.IntegerField("Fizetett összeg", blank=True, null = True)
    trid = models.IntegerField("Tranzakció id", blank=True, null = True)
    name = models.CharField("Név", max_length=100, blank=True, null = True)
    city = models.CharField("Cím Város", max_length=100, blank=True, null = True)
    rc=models.CharField("Válasz kód", max_length=100, blank=True, null = True)
    rt=models.CharField("Válasz üzenet", max_length=100, blank=True, null = True)
    adress = models.CharField("Cím utca házszám", max_length=250, blank=True, null = True)
    tellno = models.CharField("Telefonszám", max_length=250, blank=True, null = True)
    zipcode = models.CharField("Irányítószám", max_length=10, blank=True, null = True)
    
    def __unicode__(self):
        return u"%s-%s-%s" % (str(self.trid), str(self.idea), str(self.present))

    def save(self, *args, **kwargs):     
      super(BadTransaction, self).save(*args, **kwargs)
        
    class Meta:
      verbose_name = 'Hibás Tranzakció'
      verbose_name_plural = 'Hibás Tranzakciók'

class NewTenderAdmin(models.Model):
    proj = models.ForeignKey(Tender)
    STATUS_CHOISE = (('none', '--------'),('elut', 'Elutasítva'),('felt', 'Feltételes'),('elfo', 'Elfogadva'),)
    activ_status =  models.CharField(max_length=4, choices=STATUS_CHOISE, blank=True, null=True)
    how_stand = models.TextField("Hogany állunk?", max_length=1000, blank=True, null = True)
    comment = models.TextField("Megjegyzés", max_length=1000, blank=True, null = True)

    def save(self, *args, **kwargs):     
      super(NewTenderAdmin, self).save(*args, **kwargs)
        
    class Meta:
      verbose_name = 'Új Projektek'
      verbose_name_plural = 'Új Projektek'
    
#######################################################################
# BUBBLE TEXTS       
#######################################################################
class BubbleText(models.Model):
   title = models.CharField("Title", max_length=200)
   link = models.CharField("Link", max_length=200, blank = True)
   content = models.TextField("Content", max_length=1000)

   def __unicode__(self):
       return self.title 

   def save(self, *args, **kwargs):
       if not self.link:
         self.link = slugify(u"%s" % self.title)
       super(BubbleText, self).save(*args, **kwargs)
       sitemap_ping()

   class Meta:
      verbose_name = 'Buborék'
      verbose_name_plural = 'Buborékok'       


"""       
class NeccCategoryImage(models.Model):
    image = models.ForeignKey(NeccCategory)  
    file = models.FileField("Fájl", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)

    def __unicode__(self):
        return u"%s" % str(self.file).rsplit('/', 1)[-1]

    class Meta:
      verbose_name = 'Csatolmány'
      verbose_name_plural = 'Csatolmányok'  
         
     
   
class NeccCompany(models.Model):
  link = models.CharField(u"link", max_length=200, blank = True)
  created = models.DateTimeField(u"létrehozva", default=datetime.datetime.now)
  last_login = models.DateTimeField(_('last login'), blank=True, null=True)


  category = models.ForeignKey(NeccCategory)
  companyname = models.CharField(u"cégnév*", max_length=200)    
  description = models.TextField(u"rövid leírás a cégről*", max_length=1000)
  email = models.CharField(u"email cím*", max_length = 1000)
  website = models.CharField(u"weboldal*", max_length = 1000)
  password = models.CharField(u"jelszó*", max_length = 1000)
  password_raw = models.CharField(u"jelszó*", max_length = 1000)
  
  contactperson = models.CharField(u"kapcsolattartó neve", max_length=200, blank = True, null = True)    
  phone = models.CharField(u"telefonszám", blank = True, null = True, max_length=200)    
  facebook_page = models.CharField(u"facebook oldal", blank = True, null = True, max_length=400)    
  twitter = models.CharField(u"twitter elérhetősége", blank = True, null = True, max_length=400)    
  newletter = models.BooleanField(u"hírlevél", default=False)
  cooperate = models.BooleanField(u"szívesen részt veszek a tevékenységi körömnel azonos projektek, pályázatok értékelésében", default=False)

  enabled = models.BooleanField(u"engedélyezve", default=False)
  joined = models.BooleanField(u"csatlakozott", default=False)
  
  new_idea = models.BooleanField(u"új ötlet", default=False)
  newly_founded = models.BooleanField(u"újonnan finanszírozott", default=False)

  def __unicode__(self):
      return self.companyname 

  class Meta:
     verbose_name = 'Necc Cég'
     verbose_name_plural = 'Necc Cégek'
     
  @models.permalink
  def get_absolute_url(self):
      return ('creativeselector.views.necc_company_profile', [self.category.link, self.link])
     
  def save(self, *args, **kwargs):

    # if new user
    if self.id == None:
      # sets password
      self.password_raw = self.password
      self.set_password(self.password)
    if not self.link:
      self.link = slugify(u"%s" %  (self.companyname))
    
    if self.twitter and self.twitter.startswith("http://"):
      self.twitter = self.twitter.replace("http://", "")

    if self.twitter and self.twitter.startswith("https://"):
      self.twitter = self.twitter.replace("https://", "")

    if self.twitter and self.twitter.startswith("www."):
      self.twitter = self.twitter.replace("www.", "")

    if self.twitter and self.twitter.startswith("twitter.com/"):
      self.twitter = self.twitter.replace("twitter.com/", "")

    # super.save
    super(NeccCompany, self).save(*args, **kwargs) 

  def is_anonymous(self):
    """
"""
    Always returns False. This is a way of comparing User objects to
    anonymous users.
    """
"""    return False

  def is_authenticated(self):
    """
"""    Always return True. This is a way to tell if the user has been
    authenticated in templates.
    """
"""    return True

  def set_password(self, raw_password):
      import random
      algo = 'sha1'
      salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
      hsh = get_hexdigest(algo, salt, raw_password)
      self.password = '%s$%s$%s' % (algo, salt, hsh)

  def check_password(self, raw_password):
      """
"""      Returns a boolean of whether the raw_password was correct. Handles
      encryption formats behind the scenes.
      """
"""      #loggin.error("check_password")
      # Backwards-compatibility check. Older passwords won't include the
      # algorithm or salt.
      if '$' not in self.password:
          is_correct = (self.password == get_hexdigest('md5', '', raw_password))
          if is_correct:
              # Convert the password to the new, more secure format.
              self.set_password(raw_password)
              self.save()
          return is_correct
      return check_password(raw_password, self.password)

  def set_unusable_password(self):
      # Sets a value that will never be a valid hash
      self.password = UNUSABLE_PASSWORD

  def has_usable_password(self):
      return self.password != UNUSABLE_PASSWORD

  def get_and_delete_messages(self):
      messages = []
      for m in self.message_set.all():
          messages.append(m.message)
          m.delete()
      return messages
    
  def _get_message_set(self):
      import warnings
      warnings.warn('The user messaging API is deprecated. Please update'
                    ' your code to use the new messages framework.',
                    category=PendingDeprecationWarning)
      return self._message_set
  message_set = property(_get_message_set)
  
class NeccCompanyImage(models.Model):
    image = models.ForeignKey(NeccCompany)  
    file = models.FileField("Fájl", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)

    def __unicode__(self):
        return u"%s" % str(self.file).rsplit('/', 1)[-1]

    class Meta:
      verbose_name = 'Csatolmány'
      verbose_name_plural = 'Csatolmányok'   
     
class Projekt(models.Model):
  link = models.CharField("Link", max_length=200, blank = True)
  created = models.DateTimeField("Létrehozva", default=datetime.datetime.now)
  modified = models.DateTimeField("Módosítva", blank=True, null = True)
  fromDate = models.DateTimeField("Kezdés", default=datetime.datetime.now)
  toDate = models.DateTimeField("Vég", default=datetime.datetime.now)
  title = models.CharField("Cím", max_length=200, blank = True)
  lead = models.TextField("Bevezető", max_length=1000, blank = True)
  description = models.TextField("Leírás", max_length=2000, blank = True)
  active = models.BooleanField("Aktív?", default = False)
  
  def __unicode__(self):
      return u"%s"%(self.link)
  
  def save(self, *args, **kwargs):
    super(Projekt, self).save(*args, **kwargs)
    if not self.link:
      self.link = slugify(u"%s" % (self.title))
    
    super(Projekt, self).save(*args, **kwargs)

class ProjektFile(models.Model):
  files = models.ForeignKey(Projekt)  
  file = models.FileField("Fájl", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)

  def __unicode__(self):
    return u"%s" % str(self.file).rsplit('/', 1)[-1]

  class Meta:
    verbose_name = 'Fájl'
    verbose_name_plural = 'Fájlok'       
"""
"""
#######################################################################
# Mentor       
#######################################################################

class Mentor(models.Model):
  name = models.CharField("Név", max_length=200)
  link = models.CharField("Link", max_length=200, blank = True)
  title = models.CharField("Titulus", max_length=200, blank = True)
  company = models.CharField("Cég", max_length=200, blank = True)
  description = models.TextField("Leírás", max_length=2000)
  image = models.FileField("Kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)
  
  def save(self, *args, **kwargs):
      if not self.link:
        self.link = slugify(u"%s - %s - %s" % (self.name, self.company, self.title))
      
      super(Mentor, self).save(*args, **kwargs)
      sitemap_ping()

  @models.permalink
  def get_absolute_url(self):
      return ('creativeselector.views.one_mentor', [str(self.link)])

  
  def __unicode__(self):
      return u"%s (%s - %s)" % (self.name, self.title, self.company)
      
  class Meta:
    verbose_name = 'Mentor'
    verbose_name_plural = 'Mentorok'          
      
#######################################################################
# Partner       
#######################################################################

class Partner(models.Model):
  name = models.CharField("Név", max_length=200)
  link = models.CharField("Link", max_length=200, blank = True)
  image = models.FileField("Kép", upload_to='uploads/%Y/%m/%d/%H/%M/%S/', blank = True, null = True)
  media_supporter = models.BooleanField('Médiatámogató?', default=False)
  order = models.IntegerField("Sorrend", blank=True, default=0)

  def __unicode__(self):
      return u"%s" % self.name      
      
  class Meta:
    verbose_name = 'Támogató'
    verbose_name_plural = 'Támogatók'      
"""    
"""
#############################################################
# XML writer
#############################################################
class XMLWriter:
    def __init__(self, pretty=True):
        self.output = ""
        self.stack = []
        self.pretty = pretty
        
    def open(self, tag):
        self.stack.append(tag)
        if self.pretty:
            self.output += "  "*(len(self.stack) - 1);
        self.output += "<" + tag + ">"
        if self.pretty:
            self.output += "\n"
        
    def close(self):
        if self.pretty:
            self.output += "\n" + "  "*(len(self.stack) - 1);
        try:
            tag = self.stack.pop()
        except:
            tag = ""
        self.output += "</" + tag + ">"
        if self.pretty:
            self.output += "\n"
        
    def closeAll(self):
        while len(self.stack) > 0:
            self.close()
        
    def content(self, text):
        from django.utils.encoding import smart_str, smart_unicode
        if self.pretty:
            self.output += "  "*len(self.stack);
        self.output += smart_str(text)
        
    def save(self, filename):
        self.closeAll()
        fp = open(filename, "w+")
        fp.write(self.output)
        fp.close()
"""
