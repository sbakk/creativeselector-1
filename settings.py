try:
    from djangoappengine.settings_base import *
    has_djangoappengine = True
except ImportError:
    has_djangoappengine = False
    DEBUG = True
    TEMPLATE_DEBUG = DEBUG



DEBUG = False

import os


SECRET_KEY = '=r-$b*8hglm+858&9t043hlm6-&6-3d3vfc4((7yd0dbrakhvi'

INSTALLED_APPS = (
  'djangotoolbox',
  'django.contrib.auth',
  'django.contrib.sites',
  'django.contrib.admin',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.sitemaps',
  'creativeselector',
  'django_facebook',
  'simplejson',
  'adminupload',
  'filetransfers',
  'mediagenerator',
  'tagging',
  'xlwt',
  'tagging_autocomplete',

)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
  'django.core.context_processors.request',
  "django.contrib.auth.context_processors.auth",
  "django.core.context_processors.debug",
  "django.core.context_processors.i18n",
  "django.core.context_processors.media",
  "django.contrib.messages.context_processors.messages",
) 

AUTHENTICATION_BACKENDS = (
    'django_facebook.auth_backends.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_PROFILE_MODULE = "creativeselector.FacebookProfile"


##########################################
# MEDIA GENERATOR SETTINGS
##########################################

MEDIA_BUNDLES = (
    ('main.css',
        'reset.sass',
        'main6.sass',
        'nyroModal.sass',
        'menu2.sass',
        'share.sass',
        'banner2.sass',
        'blog2.sass',
        'prizes.sass',
        'mentors.sass',
        'messages.sass',
        'content5.sass',
        'ideas4.sass',
        'lightbox.sass',
        'facebook.sass',
        'counter.sass',
        'fresh_ideas.sass',
        'necc.sass',
        'jquery-ui-1.8.9.custom.css',
        'jquery.autocomplete.sass',
        'projekt.sass'
    ),
    ('main.js',
        'jquery-1.4.2.min.js',
        'jquery.log.js',
        'messages.js',
        'jquery.dropdownPlain.js',
        'cufon-yui.js',
        'Akbar_4002.font.js',
        'jquery.lightbox.js',
        'jquery.bubble.js',
        'jquery.popupWindow.js',
        'jquery.nyroModal-1.6.2.js',
        'jquery.jfeed.js',
        'jquery.imgpreload.min.js',
        'ajaxer2.js',
        'jquery.countdown.js',
        'jquery-ui-1.8.9.custom.min.js',
        'jquery.autocomplete.pack.js'
    ),
)

ROOT_MEDIA_FILTERS = {
    'js': 'mediagenerator.filters.yuicompressor.YUICompressor',
    'css': 'mediagenerator.filters.yuicompressor.YUICompressor',
}

YUICOMPRESSOR_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)),'yuicompressor.jar')

MEDIA_DEV_MODE = DEBUG
PRODUCTION_MEDIA_URL = '/media/generated/'
if MEDIA_DEV_MODE:
    MEDIA_URL = '/devmedia/'
else:
    MEDIA_URL = PRODUCTION_MEDIA_URL
    
SASS_DEBUG_INFO = True    

RSS_SOURCE = "http://creativeselector.posterous.com/rss.xml?page=%s" 
#"http://creativeselector.posterous.com/rss.xml?page=%s"

##########################################
# ENDOF MEDIA GENERATOR SETTINGS
##########################################

CACHE_MIDDLEWARE_SECONDS = 3600


if has_djangoappengine:
    INSTALLED_APPS = ('djangoappengine',) + INSTALLED_APPS

ADMIN_MEDIA_PREFIX = '/media/admin/'
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media')
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), 'templates'),)

SETTINGS_FILE_FOLDER = MEDIA_ROOT

GLOBAL_MEDIA_DIRS = (
          os.path.join(MEDIA_ROOT, 'css'), 
          os.path.join(MEDIA_ROOT, 'js'),
          os.path.join(MEDIA_ROOT, 'images'),
          os.path.join(MEDIA_ROOT, 'images/bp_images'),
          os.path.join(MEDIA_ROOT, 'images/lightbox'),
          os.path.join(MEDIA_ROOT, 'images/nyro'),
          os.path.join(MEDIA_ROOT, 'images/prizes'),
          os.path.join(MEDIA_ROOT, 'images/jquery_ui'),
                )

GENERATE_MEDIA_DIR = os.path.join(MEDIA_ROOT, 'generated')

MEDIA_PREFIX = "/media/"

ROOT_URLCONF = 'urls'

BLOG_PAGING = 8
IDEA_PAGING = 8
NUM_OF_RELATED = 10

SITE_ID = 282003
#SITE_ID = 249004

############################
# FACEBOOK SETTINGS
############################
# LIVE SETTINGS
FACEBOOK_APP_ID = "145204418841370" #"187122974660121" #"b8b01d46e1d7f4850ca74984e3201978"
FACEBOOK_APP_SECRET = "1a4cd090970032a7bd43ccd93038aac3" #"e3e9185200ab1fad9a4cf98b2358649c"
FACEBOOK_FAKE_PASSWORD = True

# LOCAL SETTINGS
#FACEBOOK_APP_ID="acf6e2957783c62ba01756efadb80559"
#FACEBOOK_APP_SECRET="276a2b12723e25e2aceeeb021d34b340"

# TEST SETTINGS
#FACEBOOK_APP_ID= "b28b1cbc65d8747f7326c4817dcb4a85" #"62d317c3c7d56f9a62f2c38ee5b07059"
#FACEBOOK_APP_SECRET= "e3e9185200ab1fad9a4cf98b2358649c" #"00f97f3187e5abcb21e038bb7da8a1b0"



############################
# RECPATHCHA SETTINGS
############################
RECAPTCHA_PUB_KEY = "6LcFkLoSAAAAALydnILgjd0EOfpo1j547lqHVM1-"
RECAPTCHA_PRIVATE_KEY = "6LcFkLoSAAAAAJxTiX1aqgc87p924akd-2Di48cr"

# TEST IN VMWARE
#RECAPTCHA_PUB_KEY = "6LeJRLsSAAAAAAh9QCbrVYx-ODnGDBfXhCEVIqSq"
#RECAPTCHA_PRIVATE_KEY = "6LeJRLsSAAAAAEbHTxKkHXZDxAzTkITgi2nfVTWg"

############################
# ACTUAL PROJEKT ID
############################
ACTUAL_PROJEKT_ID = "adecco"
